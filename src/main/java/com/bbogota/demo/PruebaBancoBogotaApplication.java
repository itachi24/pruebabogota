package com.bbogota.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaBancoBogotaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaBancoBogotaApplication.class, args);
	}

}
