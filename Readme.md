Pasos para ejecutar backend desde netbeans.

1. Configurar la aplicación para que funcione con el servidor de aplicaciones wildfly.
2. Click derecho sobre el proyecto y click en la opcion clean and build.
3. Click derecho sobre el proyecto y click en la opción deploy o ejecutar.

Nota: Debe ejecutarse con una version de java 1.8 ya que se programo con dicha versión.
